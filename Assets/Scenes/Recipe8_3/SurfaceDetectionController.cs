﻿using System.Collections.Generic;
using GoogleARCore;
using UnityEngine;
using UnityEngine.Rendering;
using GoogleARCore.Examples.Common;

public class SurfaceDetectionController : MonoBehaviour
{

    public GameObject TrackedPlanePrefab;
    public PolyImportManager polyImportManager;

    private List<DetectedPlane> newPlanes = new List<DetectedPlane>();

    public void Update()
    {
        Session.GetTrackables<DetectedPlane>(newPlanes, TrackableQueryFilter.New);
        for (int i = 0; i < newPlanes.Count; i++)
        {
            GameObject planeObject = Instantiate(TrackedPlanePrefab,
                Vector3.zero,
                Quaternion.identity,
                transform);
            planeObject.GetComponent<DetectedPlaneVisualizer>().Initialize(newPlanes[i]);
        }

        Touch touch;
        if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase
                != TouchPhase.Began)
        {
            return;
        }

        // Raycast against the location the player touched to
        //search for planes.
        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;
        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
        {
            // Hide the planes and place the Poly object
            HidePlanes();
            GameObject polyObject = polyImportManager.polyObject;
            polyObject.transform.position = hit.Pose.position;
            Anchor anchor = hit.Trackable.CreateAnchor(hit.Pose);
            polyObject.transform.parent = anchor.transform;
            polyObject.SetActive(true);
        }
    }

    private void HidePlanes()
    {
        GameObject[] planes = GameObject.
            FindGameObjectsWithTag("DetectedPlaneViz");
        foreach (GameObject plane in planes)
        {
            plane.SetActive(false);
        }
    }
}