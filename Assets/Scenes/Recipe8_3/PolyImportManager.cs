﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PolyToolkit;

public class PolyImportManager : MonoBehaviour
{

    public Text polyStatusText;

    [System.NonSerialized]
    public GameObject polyObject;

    void Start()
    {
        polyStatusText.text = "Loading Poly Object.";

        // Get the Poly Asset
        PolyApi.GetAsset("assets/1HpVP5w2x1D", GetAssetCallback);
    }

    // Invoked when the Poly asset is loaded
    private void GetAssetCallback(PolyStatusOr<PolyAsset> result)
    {
        if (!result.Ok)
        {
            Debug.LogError("Failed to get assets. Reason: " + result.Status);
            polyStatusText.text = "ERROR: " + result.Status;
            return;
        }

        PolyImportOptions options = PolyImportOptions.Default();
        options.rescalingMode = PolyImportOptions.RescalingMode.FIT;
        options.desiredSize = 0.25f;
        polyStatusText.text = "Importing...";

        // Import the asset's GameObject into the scene
        PolyApi.Import(result.Value, options, ImportAssetCallback);
    }

    // Invoked when an asset's GameObject has just been imported.
    private void ImportAssetCallback(PolyAsset asset, PolyStatusOr<PolyImportResult> result)
    {
        if (!result.Ok)
        {
            Debug.LogError("Failed to import asset. :( Reason: " + result.Status);
            polyStatusText.text = "ERROR: Import failed: "+ result.Status;
            return;
        }

        // Show attribution (asset title and author).
        polyStatusText.text = asset.displayName + "\nby " + asset.authorName;
        polyObject = result.Value.gameObject;
        polyObject.SetActive(false);
    }
}