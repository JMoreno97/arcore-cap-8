﻿using UnityEngine;
using PolyToolkit;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ThumbItemController : MonoBehaviour, IPointerClickHandler
{

    private Image thumbImage;
    private PolyAsset polyAsset;

    void Start()
    {
        thumbImage = GetComponent<Image>();
    }

    public void LoadThumb(PolyAsset asset)
    {
        PolyApi.FetchThumbnail(asset, FetchThumbnailComplete);
    }

    void FetchThumbnailComplete(PolyAsset asset, PolyStatus status)
    {

        if (!status.ok)
        {
            // Handle error;
            return;
        }
        polyAsset = asset;
        Texture2D tex = polyAsset.thumbnailTexture;
        Sprite sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        thumbImage.sprite = sprite;
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        PolyImportManagerV2.instance.UpdatePolyAsset(polyAsset);
    }
}