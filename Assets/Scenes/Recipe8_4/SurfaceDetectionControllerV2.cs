﻿using System.Collections.Generic;
using GoogleARCore;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using GoogleARCore.Examples.Common;

public class SurfaceDetectionControllerV2 : MonoBehaviour
{

    public GameObject TrackedPlanePrefab;
    public GameObject SearchingForPlaneUI;
    public Text debugtext;

    private List<DetectedPlane> newPlanes = new List<DetectedPlane>();
    private bool m_IsQuitting = false;

    // The height of the UI scrollView and status text
    private const float uiHeight = 460f;

    public void Update()
    {
        if (Session.Status != SessionStatus.Tracking)
        {
            const int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
            return;
        }

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        Session.GetTrackables<DetectedPlane>(newPlanes, TrackableQueryFilter.New);
        for (int i = 0; i < newPlanes.Count; i++)
        {
            GameObject planeObject = Instantiate(TrackedPlanePrefab, Vector3.zero, Quaternion.identity, transform);
            planeObject.GetComponent<DetectedPlaneVisualizer>().Initialize(newPlanes[i]);
        }

        Touch touch;
        if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase!= TouchPhase.Began)
        {
            return;
        }

        TrackableHit hit;
        TrackableHitFlags raycastFilter =TrackableHitFlags.PlaneWithinBounds |TrackableHitFlags.PlaneWithinPolygon;
        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
        {
            // If the touch is on a plane and not on the UI
            if (PolyImportManagerV2.instance.polyObject != null && touch.position.y > uiHeight)
            {
                debugtext.text = "Instanciando objeto";
                HidePlanes();
                // Place the Poly object
                PolyImportManagerV2.instance.PlacePolyObject(hit);
            }
            else
            {
                debugtext.text = "Objeto nulo";
            }
        }
    }

    private void HidePlanes()
    {
        GameObject[] planes = GameObject.FindGameObjectsWithTag("DetectedPlaneViz");
        foreach (GameObject plane in planes)
        {
            plane.SetActive(false);
        }
    }
}