﻿using UnityEngine;
using UnityEngine.UI;
using PolyToolkit;
using GoogleARCore;

public class PolyImportManagerV2 : MonoBehaviour
{

    public static PolyImportManagerV2 instance = null;
    public Text polyStatusText;
    public PolyScrollViewController polyScrollViewController;

    [System.NonSerialized]
    public GameObject polyObject;

    private PolyAsset polyAsset;

    // Implement the singleton pattern
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        polyStatusText.text = "Loading Thumbnails.";
        FetchPolyListAssets();
    }

    // Create a custom request to retrieve 20 chair thumbnails
    private void FetchPolyListAssets()
    {
        PolyListAssetsRequest req = new PolyListAssetsRequest();
        req.keywords = "chair";
        req.curated = true;
        req.maxComplexity = PolyMaxComplexityFilter.MEDIUM;
        req.formatFilter = PolyFormatFilter.BLOCKS;
        req.orderBy = PolyOrderBy.BEST;
        req.pageSize = 20;
        PolyApi.ListAssets(req, ListAssetsComplete);
    }

    // Returns List of PolyAssets - data objects
    private void ListAssetsComplete(PolyStatusOr<PolyListAssetsResult> result)
    {
        if (!result.Ok)
        {
            polyStatusText.text = "ERROR: Import failed: "
                + result.Status;
            return;
        }

        polyScrollViewController.LoadPolyThumbs(result.Value.assets);
        polyStatusText.text = "Thumbnails Loaded.";
    }

    // Called when user taps scrollview thumbnail to select an object
    public void UpdatePolyAsset(PolyAsset asset)
    {
        polyAsset = asset;
        polyStatusText.text = "Importing Asset";

        PolyImportOptions options = PolyImportOptions.Default();
        options.rescalingMode = PolyImportOptions.RescalingMode.FIT;
        options.desiredSize = 1f;
        PolyApi.Import(polyAsset, options, ImportAssetCallback);
    }

    // Callback invoked when an asset has just been imported.
    private void ImportAssetCallback(PolyAsset asset,
        PolyStatusOr<PolyImportResult> result)
    {
        if (!result.Ok)
        {
            Debug.LogError("Failed to import asset. :( Reason: "
                + result.Status);
            polyStatusText.text = "ERROR: Import failed: "
                + result.Status;
            return;
        }

        // Show attribution (asset title and author).
        polyStatusText.text = asset.displayName + "\nby " + asset.authorName;
        polyObject = result.Value.gameObject;
        polyObject.SetActive(false);
    }

    // Places the object at the hit location
    public void PlacePolyObject(TrackableHit hit)
    {
        Vector3 polyPos = new Vector3(hit.Pose.position.x, hit.Pose.position.y, hit.Pose.position.z);
        polyObject.transform.position = polyPos;
        var anchor = hit.Trackable.CreateAnchor(hit.Pose);
        polyObject.transform.parent = anchor.transform;
        polyObject.SetActive(true);
    }
}