﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections.Generic;
using UnityEngine;
using PolyToolkit;

public class PolyScrollViewController : MonoBehaviour
{

    public GameObject thumbItemPrefab;
    public GameObject horizontalLayout;

    public void LoadPolyThumbs(List<PolyAsset> assets)
    {
        foreach (PolyAsset asset in assets)
        {
            GameObject thumbItem = Instantiate(thumbItemPrefab);
            thumbItem.transform.parent = horizontalLayout.transform;
            ThumbItemController thumbController =thumbItem.GetComponent<ThumbItemController>();
            thumbController.LoadThumb(asset);
        }
    }
}
