﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using GoogleARCore;

public class DrawingManager : MonoBehaviour
{

    public GameObject canvas;
    public Material whiteMat;
    public GameObject drawObjectPrefab;

    private LineRenderer currentLineRend;
    private List<Vector3> linePath = new List<Vector3>();
    private bool isDrawing = false;
    private bool hasTrackedPlane = false;
    private Anchor drawAnchor;

    public void Update()
    {
        if (!hasTrackedPlane)
        {
            List<DetectedPlane> newPlanes = new List<DetectedPlane>();
            Session.GetTrackables<DetectedPlane>(newPlanes, TrackableQueryFilter.New);
            for (int i = 0; i < newPlanes.Count; i++)
            {
                DetectedPlane dPlane = newPlanes[i];
                drawAnchor = dPlane.CreateAnchor(dPlane.CenterPose);
                hasTrackedPlane = true;
                canvas.SetActive(false);
                break;
            }
        }

        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                CreateDrawObject();
                isDrawing = true;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                linePath.Clear();
                isDrawing = false;
            }

            if (isDrawing)
            {
                DrawLine();
            }
        }
    }

    private void CreateDrawObject()
    {
        Pose cameraPose = new Pose(Frame.Pose.position, Quaternion.identity);
        GameObject drawObject = (GameObject)Instantiate(drawObjectPrefab, Frame.Pose.position, Quaternion.identity, drawAnchor.transform);
        currentLineRend = drawObject.GetComponent<LineRenderer>();
    }

    private void DrawLine()
    {
        Vector3 camDirect = Frame.Pose.rotation * Vector3.forward;
        Vector3 screenPosition = Frame.Pose.position;
        Ray ray = new Ray(screenPosition, camDirect);
        Vector3 newPos = ray.GetPoint(.1f);

        linePath.Add(newPos);
        currentLineRend.widthMultiplier = 0.01f;
        currentLineRend.positionCount = linePath.Count;
        currentLineRend.SetPosition(linePath.Count - 1, newPos);
    }
}